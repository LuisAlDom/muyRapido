//
//  ViewController.swift
//  TapMeFast
//
//  Created by IDS Comercial on 14/12/17.
//  Copyright © 2017 IDS Comercial. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var startGameButton: UIButton!
    
    var timer = Timer()
    
    var timeInt = 10    //Iniciador de tiempo en 0
    var scoreInt = 0    //Iniciador del score en 0
    var gameInt = 0     //Cuando inicie el juego en 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func startGame(_ sender: Any) {
        
        if timeInt == 10{
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(startCounter), userInfo: nil, repeats: true)
            
            startGameButton.isEnabled = false
            startGameButton.alpha = 0.5
            
        } //Va a hacer q descienda en 1 por la funcion startCounter
        
        if gameInt == 1 {
            
            scoreInt += 1
            scoreLabel.text = String(scoreInt)
            
        }else{//Hara q cada  q se presione el boton valla sumando uno en la label de score
            
            timeInt = 10
            scoreInt = 0
            
            timeLabel.text = String(timeInt)
            scoreLabel.text = String(scoreInt)
            
            startGameButton.setTitle("START", for: UIControlState.normal)
            
        }//De lo contrario colocara las etiquetas como al principio
        
    }
    
    @objc func startCounter(){
        
        timeInt -= 1
        timeLabel.text = String(timeInt) //Se resta 1  cada q se llama la funcion y se coloca en el label
        
        gameInt = 1 //Asignamos 1 a gameInt
        
        startGameButton.isEnabled = true
        startGameButton.alpha = 1
        
        startGameButton.setTitle("TAP", for: UIControlState.normal)
        
        if timeInt == 0 {
            
            timer.invalidate()
            startGameButton.isEnabled = false
            startGameButton.alpha = 0.5
            
            Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(restart), userInfo: nil, repeats: false)//Despues de 4 segundos q termine el conteo en cero lo vuelve a activar el boton
            
            startGameButton.setTitle("RESTART", for: UIControlState.normal)
        }
        
    }
    
    @objc func restart(){
        
        gameInt = 0
        startGameButton.isEnabled = true
        startGameButton.alpha = 1
    }//Hace q se vuelva a habilitar el boton
    
}

